package com.kubbo.app.product.microservice.models.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.kubbo.app.product.microservice.models.entities.Product;

public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {
	public List<Product> findAllByDeletedFalse();
	public Page<Product> findAllByDeletedFalse(Pageable pageable);
	public List<Product> findAllByDeletedFalseAndEnabledTrue();
	public List<Product> findByIdIn(List<Long> ids);
}
