package com.kubbo.app.product.microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class KubboProductMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(KubboProductMicroserviceApplication.class, args);
	}

}
