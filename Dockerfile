FROM openjdk:11

VOLUME /tmp

ADD ./target/kubbo-product-microservice-0.0.1-SNAPSHOT.jar kubbo-product-microservice.jar

ENTRYPOINT ["java", "-jar", "/kubbo-product-microservice.jar"]