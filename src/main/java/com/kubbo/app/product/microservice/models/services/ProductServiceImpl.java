package com.kubbo.app.product.microservice.models.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kubbo.app.product.microservice.models.entities.Product;
import com.kubbo.app.product.microservice.models.repositories.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository repository;
	
	@Override
	@Transactional(readOnly = true)
	public List<Product> findAllByDeletedFalse() {
		return repository.findAllByDeletedFalse();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Product> findAllByDeletedFalse(Pageable pageable) {
		return repository.findAllByDeletedFalse(pageable);
	}

	@Override
	@Transactional
	public Product save(Product product) {
		return repository.save(product);
	}

	@Override
	@Transactional(readOnly = true)
	public Product findById(Long id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public boolean deleteById(Long id) {
		Optional<Product> op = repository.findById(id);
		
		if (!op.isEmpty()) {
			op.get().setDeleted(true);
			repository.save(op.get());
			
			return true;
		}
		
		return false;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Product> findAllByDeletedFalseAndEnabledTrue() {
		return repository.findAllByDeletedFalseAndEnabledTrue();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Product> findByIdIn(List<Long> ids) {
		return repository.findByIdIn(ids);
	}
}
