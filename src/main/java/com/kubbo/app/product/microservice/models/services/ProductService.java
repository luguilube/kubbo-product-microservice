package com.kubbo.app.product.microservice.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.kubbo.app.product.microservice.models.entities.Product;

public interface ProductService {
	public List<Product> findAllByDeletedFalse();
	public Page<Product> findAllByDeletedFalse(Pageable pageable);
	public List<Product> findAllByDeletedFalseAndEnabledTrue();
	public Product save(Product product);
	public Product findById(Long id);
	public boolean deleteById(Long id);
	public List<Product> findByIdIn(List<Long> ids);
}
