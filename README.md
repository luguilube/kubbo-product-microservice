## Kubbo Test App

Kubbo Test App es una app basada en una idea inicial de la empresa Kubbo, específicamente como prueba técnica para postulación a una vacante. Esta app fue diseñada en un principio por un conjunto de 6 microservicios para el backend  (servidor, customers, products, warehouses, orders, cloud gateway) para gestionar específicamente lo referente a cada funcionalidad requerida por la App, pero segmentada en módulos; y para el frontend un proyecto únicamente. Actualmente se sigue actualizando que nuevas funcionalidades y tecnologías para ser mostrada como proyecto de portafolio.

## Descripción del Proyecto

Toda la descripción detallada del proyecto, se encuentra dentro del repositorio de [kubbo-test-app-frontend](https://bitbucket.org/luguilube/kubbo-test-app-frontend/src/master/) 

## Kubbo Product Microservice

Microservicio que permite todas las operaciones CRUD para la entidad Productos, y el cual es el pilar del proyecto ya que dependen de él los almacenes y los envíos.

## Nota

Se recomienda que este microservicio, sea el quinto puesto que que aunque no está relacionado directamente con la entidad Cliente o Usuarios, si se recomienda se ejecute luego de estos; 

Este microservicio depende de la ejecución de los microservicios:

- kubbo-config-server.
- kubbo-eureka-server.