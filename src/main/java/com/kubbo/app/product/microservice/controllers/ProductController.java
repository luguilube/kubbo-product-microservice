package com.kubbo.app.product.microservice.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kubbo.app.product.microservice.models.entities.Product;
import com.kubbo.app.product.microservice.models.services.ProductService;

@RestController
public class ProductController {
	private static final Logger log = LoggerFactory.getLogger(ProductController.class);
	private final ProductService service;

	@Autowired
	public ProductController(ProductService service) {
		this.service = service;
	}
	
	@GetMapping("/list")
	public ResponseEntity<?> list() {
		try {
            return ResponseEntity.ok().body(service.findAllByDeletedFalse());
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
	}
	
	@GetMapping("/paginated-list")
    public ResponseEntity<?> paginatedList(Pageable pageable) {
        try {
            return ResponseEntity.ok().body(service.findAllByDeletedFalse(pageable));
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }

    @PostMapping("store")
    public ResponseEntity<?> store(@Valid @RequestBody Product product, BindingResult result) {
        try {
            if (result.hasErrors()) {
                return validation(result);
            }

            return ResponseEntity.status(HttpStatus.CREATED).body(service.save(product));
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<?> get(@PathVariable Long id) {
        try {
            Product product = service.findById(id);

            if (product == null) {
                return ResponseEntity.notFound().build();
            }

            return ResponseEntity.ok().body(product);
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }
    
    @PutMapping("update/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody Product product, BindingResult result, @PathVariable Long id) {
        try {
            if (result.hasErrors()) {
                return validation(result);
            }
            
            Product productToUpdate = service.findById(id);
            
            if (productToUpdate == null) {
            	return ResponseEntity.notFound().build();
            }
            
            productToUpdate.setSku(product.getSku());
            productToUpdate.setName(product.getName());
            productToUpdate.setDescription(product.getDescription());
            productToUpdate.setEnabled(product.getEnabled());
            
            return ResponseEntity.status(HttpStatus.CREATED).body(service.save(productToUpdate));
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }


    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        try {
        	return service.deleteById(id) 
            		? ResponseEntity.noContent().build() 
    				: ResponseEntity.notFound().build();
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }

    private ResponseEntity<?> validation(BindingResult result) {
        Map<String, Object> errors = new HashMap<>();

        result.getFieldErrors().forEach(err -> {
            errors.put(err.getField(), "El campo " + err.getField() + " " + err.getDefaultMessage());
        });

        return ResponseEntity.badRequest().body(errors);
    }
    
	@GetMapping("/get-enable-products")
    public ResponseEntity<?> getEnableProducts() {
        try {
            return ResponseEntity.ok().body(service.findAllByDeletedFalseAndEnabledTrue());
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }
	
	@PostMapping("/get-products-by-ids-list")
    public ResponseEntity<?> getProductsByIdsList(@RequestBody List<Long> ids) {
        try {
            return ResponseEntity.ok().body(service.findByIdIn(ids));
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }
}
